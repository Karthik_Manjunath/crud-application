package com.h2app.service;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.h2app.domain.Restaurant;
import com.h2app.domain.User;
import com.h2app.repository.RestaurantRepository;
import com.h2app.repository.UserRepository;

@Service
public class DataLoader {
	private UserRepository userRepository;
	private RestaurantRepository restaurantRepository;
	
	@Autowired
	public DataLoader(UserRepository userRepository, RestaurantRepository restaurantRepository) {
		this.userRepository = userRepository;
		this.restaurantRepository = restaurantRepository;
	}
	
	@PostConstruct
	private void loadData() {
		User user = new User("Karan Mehra");
		user.setAddress("Banashankari");
		user.setCard((long) 1234567);
		user.setEmail("karan@aol.com");		
		userRepository.save(user);
		
		Restaurant restaurant = new Restaurant("Kakal Kai Ruchi");
		restaurant.setAddress("Gauravanagar");
		restaurant.setPhone_number(944983059);
		restaurantRepository.save(restaurant);
	}
	
}
