package com.h2app.repository;

import org.springframework.data.repository.CrudRepository;

import com.h2app.domain.Restaurant;

public interface RestaurantRepository extends CrudRepository<Restaurant, Long>{

}
