package com.h2app.repository;

import org.springframework.data.repository.CrudRepository;

import com.h2app.domain.User;

public interface UserRepository extends CrudRepository<User, Long> {
	
}
